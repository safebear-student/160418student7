Feature: task management

  User Story:
  In order to manage my tasks
  As a user
  I must be able to view, add, archive or update  task.




  Scenario Outline: A user creates a task
    When a user creates a <task>
    Then the <task> appears in the list
    Examples:
    |task|
    |Create documentation|


