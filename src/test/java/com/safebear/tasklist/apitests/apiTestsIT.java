package com.safebear.tasklist.apitests;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;

public class apiTestsIT {
    final String Domain = System.getProperty("domain");
    final int Port = Integer.parseInt(System.getProperty("port"));


    @Before
    public void setUp(){
        //RestAssured.baseURI = DOMAIN;
        //RestAssured.port = PORT;


        RestAssured.registerParser("application/json", Parser.JSON);

    }

}
