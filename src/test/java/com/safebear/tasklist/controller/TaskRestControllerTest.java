package com.safebear.tasklist.controller;


import com.safebear.tasklist.service.TaskService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@WebMvcTest
@RunWith(SpringRunner.class)
public class TaskRestControllerTest {

    private TaskController taskController;

    private MockMvc mockMvc;

    @MockBean
    private TaskService taskService;

    @Before
    public void setUp(){

        taskController = new TaskController(taskService);
        mockMvc = MockMvcBuilders.standaloneSetup(taskController).build();
    }
    


    @Test
    public void testTaskList() throws Exception{

        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/tasks"))
                     .andDo(MockMvcResultHandlers.print())
                     .andExpect(MockMvcResultMatchers.status().isOk());

    }
    @Test
    public void testSaveTask() throws Exception{

        this.mockMvc.perform(MockMvcRequestBuilders.post("/api/tasks/save")
                      .contentType(MediaType.APPLICATION_JSON)
                      .content("{\"name\":\"Configure Jenkins\",\"completed\":\"false\",\"dueDate\":\"05/04/2018\"}"))
                       .andDo(MockMvcResultHandlers.print())
                       .andExpect(MockMvcResultMatchers.status().isOk());
    }





}
